from flask import Flask
from flask.ext.assets import Environment, Bundle
from flaskext.markdown import Markdown
from blog.Blog.helpers import slugify, dt_format

app = Flask(__name__)
app.config.from_envvar('BLOG_SETTINGS')
Markdown(app)

# minify and bundle the css
assets = Environment(app)
css = Bundle('css/style.css', filters='cssmin', output='css/style.min.css')
assets.register('css_main', css)

# Register a jijna filter.
app.jinja_env.filters['reversed'] = reversed
app.jinja_env.filters['slugify'] = slugify
app.jinja_env.filters['dtformat'] = dt_format
# This should always be the last statement!
import blog.Blog.views
