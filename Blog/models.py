from blog.Blog import app
from flask.ext.sqlalchemy import SQLAlchemy
from datetime import datetime
import pytz

db = SQLAlchemy(app)

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80))
    body = db.Column(db.Text, default="")
    draft = db.Column(db.Boolean(), index=True, default=True)
    post_on = db.Column(db.DateTime(timezone=pytz.timezone('UTC')))
    updated_on = db.Column(db.DateTime(timezone=pytz.timezone('UTC')))
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    category = db.relationship('Category',
                               backref=db.backref('posts', lazy='dynamic'))

    def __init__(self, title, body, draft=False, category=None, post_on=None):
        self.title = title
        self.body = body
        self.draft = draft
        if post_on is None:
            post_on = datetime.utcnow()
        self.post_on = post_on
        self.category = category

    def update_post(self, updated_on=None):
        if updated_on is not None:
            self.updated_on = updated_on

    def __repr__(self):
        return '<Post %r>' % self.title

# Instead of having a 'tags' column in the posts,
# we can define categories seperately,
# this way, we can easily query all the posts associated
# with a certain category.


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Category %r>' % self.name

# Create the database
try:
    db.create_all()
except Exception as e:
    app.logger.debug("%s - %s" % (type(e), str(e)))
