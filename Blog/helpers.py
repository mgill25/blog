#!/usr/bin/env python

# Utility methods go here.
import re
from unicodedata import normalize

_punct_re = re.compile(r'[\t !"#$%&\'()*\-/<=>?@\[\\\]^_`{|},.]+')
_datetime_format = "%B %d, %Y"

def slugify(text, delim=u'-'):
    """
    Generates an ASCII only slug.
    Won't work well with Asian symbols.
    Ref: http://flask.pocoo.org/snippets/5/
    """
    result = []
    for word in _punct_re.split(text):
        word = normalize('NFKD', word).encode('ascii', 'ignore')
        if word:
            result.append(word)
    return unicode(delim.join(result))

def dt_format(date_obj):
    """
    Nicely reformat the given datetime object.
    """
    return date_obj.strftime(_datetime_format)
