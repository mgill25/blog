# views.py
from werkzeug.contrib.atom import AtomFeed
from flask import request, redirect, url_for, \
        abort, render_template, flash, Response
from flask import Markup

from blog.Blog import app
from blog.Blog.models import db, Post, Category
from blog.Blog.helpers import slugify

from datetime import datetime
from functools import wraps
from urlparse import urljoin
import markdown
import traceback
import sys

POSTS_PER_PAGE = 5

def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    return username == app.config['USERNAME'] and \
            password == app.config['PASSWORD']


def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
        'Could not verify your access level for that URL.\n'
        'You have to login with proper credentials', 401,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})


def requires_auth(f):
    """
    Decorator. Use with any view that should
    only be visible to authenticated users.
    """
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated

def make_external(url):
    return urljoin(request.url_root, url)

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@app.route('/index/<int:page>', methods=['GET', 'POST'])
def index(page=1):
    """Show recent posts on the main page, starting from most recent."""
    try:
        # Select all posts in descending order (for pagination)
        posts = Post.query.filter_by(draft=False).order_by(Post.post_on.desc())
        posts = posts.paginate(page, POSTS_PER_PAGE, False)
        return render_template('index.html', posts=posts)
    except Exception as e:
        app.logger.debug('%s - %s' % (type(e), str(e)))
        #traceback.print_exc(file=sys.stdout)
        return redirect(url_for('page_not_found', e=404))


@app.route('/single/<post_title>', methods=['GET'])
def individual_post(post_title):
    """Show only a single post"""
    try:
        # parse slug to get original name
        post_title = ' '.join(post_title.split('-'))
        post = Post.query.filter_by(title=post_title).first()
        return render_template('single_post.html', post=post)
    except Exception as e:
        app.logger.debug('%s - %s' % (type(e), str(e)))
        traceback.print_exc(file=sys.stdout)
        return redirect(url_for('page_not_found', e=404))


@app.route('/dashboard', methods=['GET', 'POST'])
@requires_auth
def dashboard():
    all_posts = Post.query.all()
    live = [post for post in all_posts if post.draft==False]
    draft = [post for post in all_posts if post.draft]
    return render_template('dashboard.html', live=live, draft=draft)


# Add new posts.
@app.route('/add', methods=['POST'])
@requires_auth
def add():
    # app.logger.debug(request.form)
    if request.method == 'POST':
        title = ' '.join(slugify(request.form['title']).split('-'))
        body = Markup(markdown.markdown(request.form['text']))
        #print request.form.items()
        # don't show drafts on the index page
        draft = True if 'draft' in request.form else False
        category = Category(request.form['category']) if 'category' in \
                request.form else 'default'
        post_on = datetime.now()
        updated_on = post_on
        form_data = Post(title, body, draft, category, post_on)
        db.session.add(form_data)
        db.session.commit()
    else:
        app.logger.error('Invalid method!')
    return redirect(url_for('index'))


@app.route('/delete', methods=['POST'])
@requires_auth
def delete():
    """Permanently remove the given post from the database"""
    app.logger.debug(request.form['id'])
    post = Post.query.filter_by(id=request.form['id'])
    db.session.delete(post)
    db.session.commit()
    flash("Post deleted!")
    return redirect(url_for('dashboard'))


@app.route('/drafts')
@requires_auth
def drafts():
    """Query the posts where the draft field is set to True"""
    try:
        posts = Post.query.filter_by(draft=True).all()
        return render_template('drafts.html', posts=posts)
    except Exception as e:
        app.logger.debug('%s - %s' % (type(e), str(e)))

@app.route('/edit/<post_title>')
@requires_auth
def edit(post_title):
    """Open textarea to edit a post"""
    post_title = ' '.join(post_title.split('-'))
    post = Post.query.filter_by(title=post_title).first()
    return render_template('update.html', post=post)

@app.route('/update', methods=['POST'])
def update():
    """Update a post in the database"""
    pass

@app.route('/code')
def code():
    return render_template('code.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/contact')
def contact():
    return render_template('contact.html')

@app.route('/archive')
def archive():
    return render_template('archive.html')

@app.route('/feed')
def recent_feed():
    feed = AtomFeed('The Burning Code | Recent Posts', feed_url=request.url, url=request.url_root)
    posts = Post.query.order_by(Post.post_on.desc()).limit(15).all()
    for post in posts:
        url = make_external('single/' + slugify(post.title))
        updated_on = post.updated_on or post.post_on
        feed.add(post.title, unicode(post.body), content_type='html',
                author='Manish Gill', url=url, id=post.id,
                updated=updated_on, published=post.post_on)
    return feed.get_response()

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html')

